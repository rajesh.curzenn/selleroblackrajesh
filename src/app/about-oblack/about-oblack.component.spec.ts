import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutOblackComponent } from './about-oblack.component';

describe('AboutOblackComponent', () => {
  let component: AboutOblackComponent;
  let fixture: ComponentFixture<AboutOblackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AboutOblackComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AboutOblackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
