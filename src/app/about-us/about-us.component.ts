import { Component, OnInit } from '@angular/core';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  baseurl:any
  aboutAPI:any; aboutAPIRes:any; aboutAPIData:any
  constructor(private seller: SellerserviceService, private http: HttpClient) {
    this.baseurl = seller.baseapiurl2


    this.aboutAPI = this.baseurl + "api/getWebPageBySlug/seller_web_about_us"
   }

  ngOnInit(): void {

    this.http.get(this.aboutAPI).subscribe(res=>{
      this.aboutAPIRes = res
      if(this.aboutAPIRes.success){
        this.aboutAPIData = this.aboutAPIRes.data
        
      }
    })
  }

}
