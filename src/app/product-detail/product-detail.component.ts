import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SellerserviceService } from '../sellerservice.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {
  baseurl:any; productID:any
  getproductbyidtoedit:any; Apires:any; singleProduct:any
  constructor(private route:ActivatedRoute,private seller: SellerserviceService, private http: HttpClient,) {
    this.baseurl = seller.baseapiurl2
    this.productID = this.route.snapshot.queryParams["id"]

    this.getproductbyidtoedit = this.baseurl+"api/product/getproductbyidtoedit/"+this.productID
   }

  ngOnInit(): void {
    this.http.get(this.getproductbyidtoedit).subscribe(res=>{
      this.Apires = res
      if(this.Apires.status){
        this.singleProduct = this.Apires.data
        console.log(this.singleProduct)
      }
    })

  }

}
